from flask import Flask, render_template, request, redirect
from flask_mysqldb import MySQL
from flask import jsonify

app = Flask(__name__)

# Configure db
app.config['MYSQL_HOST'] = "localhost"
app.config['MYSQL_USER'] = "root"
app.config['MYSQL_PASSWORD'] = "6789"
app.config['MYSQL_DB'] = "maaizapp"

mysql = MySQL(app)
#for login
@app.route('/login',methods=['GET'])
def login():
    userDetails = request.args
    username = userDetails.get('userName')
    password = userDetails.get('password')
    cur = mysql.connection.cursor()
    resultValue = cur.execute("SELECT * FROM user2 WHERE userids=%s AND passw=%s",(username,password))
    cur.close()
    if(resultValue > 0):
        return "jwt token"
    else:
        return "not member"
#for signup
@app.route('/signup', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        userDetails = request.form
        userid = userDetails['UserId']
        phonenum = userDetails['PhoneNum']
        passwords = userDetails['Password']
        DOB = userDetails['DOB']
        cur = mysql.connection.cursor()
        cur.execute('INSERT INTO user2(userids,phNos,passw,dobs) VALUES(%s,%s,%s,%s)',(userid,phonenum,passwords,DOB))
        mysql.connection.commit()
        cur.close()
        return 'success'
    return render_template('basics.html')

# @app.route('/createSKU', methods=['GET', 'POST'])
# def createsku():
#     if request.method == 'POST':
#         userDetails = request.form
#         userid = userDetails('UserId')
#         skuname = userDetails('sku_name')
#         skucategory = userDetails('sku_category')
#         price = userDetails('price')
#         cur = mysql.connection.cursor()
#         cur.execute('INSERT INTO product1(userids,sku_name,sku_category,price) VALUES(%s,%s,%s,%s)',(userid,skuname,skucategory,price))
#         mysql.connection.commit()
#         cur.close()
#         return 'added'
#     return render_template('bases.html')
#module 2 begins: adding products to product table
@app.route('/createSKU', methods=['GET', 'POST'])
def createsku():
    if request.method == 'POST':
        userDetails = request.form
        userid = userDetails['UserId']
        skuname = userDetails['sku_name']
        skucategory = userDetails['sku_category']
        price = userDetails['price']
        cur = mysql.connection.cursor()
        cur.execute('INSERT INTO product1(userids,sku_name,sku_category,price) VALUES(%s,%s,%s,%s)',(userid,skuname,skucategory,price))
        mysql.connection.commit()
        cur.close()
        return 'added'
    return render_template('bases.html')

#Getting product details from product id(details displayed in html page)
@app.route('/product',methods=['GET'])
def productview():
    userDetails1 = request.args
    id = userDetails1.get('id')
    cur = mysql.connection.cursor()
    #sql_select_query = """select * from product1 where userids = %s"""
    resultValue2 = cur.execute("SELECT * FROM product1 WHERE userids=%s",(id,))
    print(resultValue2)
    if(resultValue2 > 0):
        userDetails2 = cur.fetchall()
        cur.close()
        return render_template('products.html',userDetails1 = userDetails2)
        #return 'hello'
    else:
        cur.close()
        return "no product found"
#for delete
@app.route('/delete', methods=['DELETE','GET'])
def delete_user():
    userDetails1 = request.args
    id = userDetails1.get('id')
    cur = mysql.connection.cursor()
    cur.execute("DELETE FROM product1 WHERE userids=%s", (id,))
    mysql.connection.commit()
    cur.close()
    return 'record deleted'
#for update
@app.route('/update',methods=['PUT','GET'])
def update_user():
    userDetails = request.args
    id = userDetails.get('id')
    skuname = userDetails.get('sku_name')
    skucategory = userDetails.get('sku_category')
    price = userDetails.get('price')
    cur = mysql.connection.cursor()
    cur.execute("UPDATE product1 SET sku_name=%s, sku_category=%s, price=%s WHERE userids=%s",(skuname,skucategory,price,id))
    mysql.connection.commit()
    cur.close()
    return 'record updated'

# @app.route('/update', methods=['PUT'])
# def update_user():
# 	try:
# 		_json = request.json
# 		_id = _json['userids']
# 		_name = _json['sku_name']
# 		_category = _json['sku_category']
# 		_price = _json['price']		
# 		# validate the received values
# 		if _name and _category and _price and _id and request.method == 'PUT':
# 			#do not save password as a plain text
# 			# save edits
# 			sql = "UPDATE product1 SET sku_name=%s, sku_category=%s, price=%s WHERE userids=%s"
# 			data = (_name, _category, _price, _id,)
# 			cur = mysql.connection.cursor()
# 			cur.execute(sql, data)
# 			mysql.connection.commit()
# 			resp = jsonify('User updated successfully!')
# 			resp.status_code = 200
# 			return resp
# 		else:
# 			return 'not found'
# 	except Exception as e:
# 		print(e)
# 	finally:
# 		cur.close()



if __name__ == '__main__':
    app.run(debug=True)